

# Add executable called "Game" that is built from the source files
# "game.cxx" The extensions are automatically found.
IF (WIN32)
	add_executable (Game WIN32 main.cpp)
ELSE()
	add_executable (Game main.cpp)
ENDIF()

#Includes only for this target - these could possibly be for all targets but...
target_include_directories (Game PUBLIC ${CMAKE_CURRENT_SOURCE_DIR})
target_include_directories (Game PUBLIC ${CMAKE_CURRENT_SOURCE_DIR}/../strings)
target_include_directories (Game PUBLIC ${CMAKE_CURRENT_SOURCE_DIR}/../networking)

# Link the executable to the Game library. Since the Hello library has
# public include directories we will use those link directories when building

#Game
target_link_libraries (Game LINK_PUBLIC strings)
target_link_libraries (Game LINK_PUBLIC networking)

## Tutorial type targets :-D
IF (WIN32)
	add_executable (TCPEchoServer WIN32 TCPEchoServer.cpp)
ELSE()
	add_executable (TCPEchoServer TCPEchoServer.cpp)
ENDIF()

#Includes only for this target - these could possibly be for all targets but...
target_include_directories (TCPEchoServer PUBLIC ${CMAKE_CURRENT_SOURCE_DIR})
target_include_directories (TCPEchoServer PUBLIC ${CMAKE_CURRENT_SOURCE_DIR}/../strings)
target_include_directories (TCPEchoServer PUBLIC ${CMAKE_CURRENT_SOURCE_DIR}/../networking)

# Link the executable to the TCPEchoClient library. Since the libraries have
# public include directories we will use those link directories when building

#TCPEchoClient
target_link_libraries (TCPEchoServer LINK_PUBLIC strings)
target_link_libraries (TCPEchoServer LINK_PUBLIC networking)

## Tutorial type targets :-D
IF (WIN32)
	add_executable (TCPEchoClient WIN32 TCPEchoClient.cpp)
ELSE()
	add_executable (TCPEchoClient TCPEchoClient.cpp)
ENDIF()

#Includes only for this target - these could possibly be for all targets but...
target_include_directories (TCPEchoClient PUBLIC ${CMAKE_CURRENT_SOURCE_DIR})
target_include_directories (TCPEchoClient PUBLIC ${CMAKE_CURRENT_SOURCE_DIR}/../strings)
target_include_directories (TCPEchoClient PUBLIC ${CMAKE_CURRENT_SOURCE_DIR}/../networking)

# Link the executable to the TCPEchoServer library. Since the libraries have
# public include directories we will use those link directories when building

#TCPEchoServer
target_link_libraries (TCPEchoClient LINK_PUBLIC strings)
target_link_libraries (TCPEchoClient LINK_PUBLIC networking)
